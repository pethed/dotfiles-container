# What is?

Container with mainly nvim dotfiles and plugins setup by Erik

## Why?

Because I wanted to test it without impacting my own setup

## How?


Edit Dockerfile ENV-part with your current user, modify any paths to match your preferences.

Build the container with `docker build -t [name-of-container:[version]`

 and start with
 
`docker run -it --rm -v $(pwd):/home/[user]/test [name-of-container]:[version to run] `

Add this to alias to autostart nvim, with plugins and the specified file:

`alias coolstuff="docker run -it --rm -v $(pwd):/home/$(whoami)/test [name-of-container]:[version to run]" `

then do `coolstuff [filename in current folder]`

preferably add an alias for this if you want to use it regulary, and maybe tune some settings to make it run seamlessly.

## To come

Maybe fix so that tmux is running as well, maybe add some more bells and whistles, maybe make a "build-box" so that you can easily switch between different dotfiles to test what you prefer.
