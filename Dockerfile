FROM alpine:latest
MAINTAINER Peter

# User config
ENV UID="1000" \
    UNAME=peter \
    GID="1000" \
    GNAME=peter \
    SHELL="/bin/zsh" \
    UHOME=/home/peter \
    DOTFILES=/opt/dotfiles


RUN apk --no-cache add sudo \
# Homedir
        && mkdir -p "${UHOME}" \
  && chown "${UID}":"${GID}" "${UHOME}" \
# User
        && echo "${UNAME}:x:${UID}:${GID}:${UNAME},,,:${UHOME}:${SHELL}" >> /etc/passwd \
        && echo "${UNAME}::17032:0:99999:7:::" >> /etc/shadow \
# no pass Sudo
        && echo "${UNAME} ALL=(ALL) NOPASSWD: ALL" > /etc/sudoers.d/${UNAME} \
  && chmod 0440 "/etc/sudoers.d/${UNAME}" \
# Create group
  && echo "${GNAME}:x:${GID}:${UNAME}" >> /etc/group

RUN apk add --no-cache \
        --repository https://dl-3.alpinelinux.org/alpine/edge/main \
        git \
        neovim \
        tmux \
        zsh


COPY run.sh /usr/local/bin/  
RUN chmod 0777 "/usr/local/bin/run.sh"
# Clone Dotfiles repo
RUN mkdir --parent /opt/dotfiles && chown ${UNAME}:${GNAME} /opt/dotfiles
USER $UNAME
RUN git clone https://github.com/gish/dotfiles.git /opt/dotfiles \
# Install and setup tmux
 && git clone https://github.com/tmux-plugins/tpm "${UHOME}/.tmux/plugins/tpm" \
# Install Vundle
 && git clone https://github.com/VundleVim/Vundle.vim.git ${UHOME}/.vim/bundle/vundle && chown -R ${UNAME}:${GNAME} ${UHOME}


# Link dotfiles
RUN mkdir --parent ${UHOME}/.config/nvim/
RUN ln -s ${DOTFILES}/zshrc ${UHOME}/.zshrc && ln -s ${DOTFILES}/tmux.conf ${UHOME}/.tmux.conf && ln -s ${DOTFILES}/vimrc ${UHOME}/.vimrc &&ln -s ${DOTFILES}/vimrc ${UHOME}/.config/nvim/init.vim
RUN nvim +PluginInstall +qall

#WORKDIR ${UHOME}/test

ENTRYPOINT ["sh", "/usr/local/bin/run.sh"]
